import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String enText = "Hello Flutter !";
String jpText = "こんにちは フラッター ！";
String cnText = "你好 颤振 ！";
String thText = "สวัสดี ฟลัตเตอร์ !";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = enText;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter App"),
          leading: IconButton(
              onPressed: () {
                setState(() {
                  displayText = enText;
                });
              },
              icon: Icon(Icons.home)),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == thText ? enText : thText;
                  });
                },
                icon: Icon(Icons.language))
          ,
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == jpText ? enText : jpText;
                  });
                },
                icon: Icon(Icons.g_translate))
            ,
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == cnText ? enText : cnText;
                  });
                },
                icon: Icon(Icons.translate))
          ],
        ),
        body: Center(
          child: Text(
              displayText,
              style: TextStyle(
                  fontSize: 32
              ),
          ),
        ),
      ),
    );
  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       //Scaffold Widget
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter App"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//               "Hello Flutter !",
//               style: TextStyle(
//                   fontSize: 32
//               ),
//           ),
//         ),
//       ),
//     );
//   }
// }